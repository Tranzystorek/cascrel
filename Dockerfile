FROM fedora:latest

RUN dnf upgrade -y \
      && dnf install -y cmake make gcc-c++ eigen3 spdlog catch-devel
