#include "utility/BatchRange.hpp"

using namespace cascrel::utility;

BatchRange::BatchRange(const RowMatrix& matrix, SizeType batchSize)
        : mMatrix(matrix),
          batchSize(batchSize) {
}

BatchRange::iterator BatchRange::begin() const {
    return {*this, 0};
}

BatchRange::iterator BatchRange::end() const {
    const SizeType nRows = mMatrix.rows();
    const SizeType unevenOffset = (nRows % batchSize > 0) ? 1 : 0;
    const SizeType nBatches = nRows / batchSize + unevenOffset;
    const SizeType endPosition = nBatches * batchSize;
    return {*this, endPosition};
}

cascrel::SizeType BatchRange::getBatchNumber() const {
    const SizeType nRows = mMatrix.rows();
    const SizeType unevenBatch = (nRows % batchSize > 0) ? 1 : 0;
    return nRows / batchSize + unevenBatch;
}

BatchRange::iterator::iterator(const BatchRange& range, SizeType position)
        : mRange(range),
          mPosition(position) {
}

BatchRange::iterator& BatchRange::iterator::operator++() {
    mPosition += mRange.batchSize;
    return *this;
}

BatchRange::iterator BatchRange::iterator::operator++(int) {
    iterator ret = *this;
    mPosition += mRange.batchSize;
    return ret;
}

bool BatchRange::iterator::operator==(const BatchRange::iterator& other) const {
    return mPosition == other.mPosition;
}

bool BatchRange::iterator::operator!=(const BatchRange::iterator& other) const {
    return mPosition != other.mPosition;
}

BatchRange::iterator::reference BatchRange::iterator::operator*() const {
    return getBatch();
}

BatchRange::iterator::pointer BatchRange::iterator::operator->() const {
    return BatchRange::iterator::pointer(getBatch());
}

BatchRange::iterator::reference BatchRange::iterator::getBatch() const {
    const SizeType nCols = mRange.mMatrix.cols();
    const SizeType distanceToEnd = mRange.mMatrix.rows() - mPosition;
    const SizeType batchSize = mRange.batchSize;
    const SizeType blockSize = (distanceToEnd < batchSize) ? distanceToEnd : batchSize;
    return mRange.mMatrix.block(mPosition, 0, blockSize, nCols);
}
