#include "CascadeCorrelationNetwork.hpp"

#include <cmath>
#include <functional>
#include <stdexcept>
#include <utility>

#include <spdlog/fmt/ostr.h>
#include <spdlog/sinks/stdout_sinks.h>

#include "HiddenHistory.hpp"
#include <utility/BatchRange.hpp>

#define MAIN_LOGGER_NAME "CascrelLogger"

using namespace cascrel;
using namespace cascrel::activation;
using namespace cascrel::initializers;
using namespace cascrel::loss;
using namespace cascrel::optimizers;
using namespace std::placeholders;

std::shared_ptr<spdlog::logger> registerLogger(const std::string& name) {
    spdlog::drop(name);
    return spdlog::stderr_logger_st(name);
}

CascadeCorrelationNetwork::CascadeCorrelationNetwork(
        SizeType inDim, SizeType outDim,
        Initializer& hiddenInitializer,
        Initializer& outputInitializer,
        Optimizer& hiddenOptimizer,
        Optimizer& outputOptimizer,
        ActivationFunction& hiddenActivation,
        ActivationFunction& outputActivation,
        LossFunction& lossFunction)
        : mHiddenInitializer(&hiddenInitializer),
          mOutputInitializer(&outputInitializer),
          mHiddenOptimizer(&hiddenOptimizer),
          mOutputOptimizer(&outputOptimizer),
          mOutputLayer(inDim + 1, outDim, outputInitializer),
          mHiddenActivation(&hiddenActivation),
          mOutputActivation(&outputActivation),
          mLossFunction(&lossFunction),
          mInputDimension(inDim + 1),
          mOutputDimension(outDim),
          mLogger(registerLogger(MAIN_LOGGER_NAME)) {
}

History
CascadeCorrelationNetwork::train(const RowMatrix& x, const RowMatrix& y,
        SizeType batchSize,
        unsigned int patience,
        Scalar tolerance,
        unsigned int maxHiddenLayers,
        Scalar maxLoss,
        unsigned int safetyEpochLimit) {
    checkInputAssertions(x);
    checkOutputAssertions(y);
    checkSampleCounts(x, y);

    History history;
    auto outputInitializerFunc = std::ref(*mOutputInitializer);

    mTotalEpochs = 0;

    mLogger->info("Training model...");

    mLogger->debug("Initial output weights:\n{}", mOutputLayer.weights());

    RowMatrix xWithBias = addBias(x);

    mLogger->debug("Training output layer on following input:\n{}", xWithBias);

    Scalar loss = trainOutputWithPatience(xWithBias, y, batchSize, history,
                                          patience, tolerance,
                                          safetyEpochLimit);

    mLogger->debug("Output weights after first training:\n{}",
                   mOutputLayer.weights());

    if (safetyEpochLimit && mTotalEpochs >= safetyEpochLimit) {
        mLogger->warn("Safety epoch limit reached. Finishing execution.");
        return history;
    }

    while (loss > maxLoss && mHiddenLayers.size() < maxHiddenLayers) {

        mLogger->info("Training new hidden candidate...");

        const SizeType hiddenInputDimension = mInputDimension
                + mHiddenLayers.size();
        layers::HiddenLayer candidate{hiddenInputDimension,
                                      *mHiddenInitializer};

        trainNewHiddenWithPatience(xWithBias, y, batchSize, history, candidate,
                                   patience, tolerance,
                                   safetyEpochLimit);

        // insert trained candidate into the network structure
        mHiddenLayers.push_back(std::move(candidate));

        if (safetyEpochLimit && mTotalEpochs >= safetyEpochLimit) {
            mLogger->warn("Safety epoch limit reached. Finishing execution.");
            return history;
        }

        //TODO preemptively allocate the output weight matrix for efficiency
        mOutputLayer.addRows(1);
        mOutputLayer.weights().bottomRows(1) = RowVector::NullaryExpr(
                mOutputDimension,
                outputInitializerFunc);

        loss = trainOutputWithPatience(xWithBias, y, batchSize, history,
                                       patience, tolerance,
                                       safetyEpochLimit);

        history.addNewRecord(loss);

        if (safetyEpochLimit && mTotalEpochs >= safetyEpochLimit) {
            mLogger->warn("Safety epoch limit reached. Finishing execution.");
            return history;
        }
    }

    return history;
}

RowVector
CascadeCorrelationNetwork::evaluate(const RowMatrix& x, const RowMatrix& y) const {
    checkInputAssertions(x);
    checkOutputAssertions(y);
    checkSampleCounts(x, y);

    auto outputActivationFunc = std::ref(*mOutputActivation);
    auto lossFunc = std::ref(*mLossFunction);

    mLogger->info("Evaluating network performance");

    RowMatrix xWithHidden = calculateHiddenOutputs(addBias(x));
    RowMatrix rawOutput = mOutputLayer.calculateOutput(xWithHidden);

    auto activatedOutput = rawOutput.unaryExpr(outputActivationFunc);

    auto lossValues = (y - activatedOutput).unaryExpr(lossFunc);

    RowVector meanLossPerOutput = lossValues.colwise().mean();

    mLogger->info("Mean loss per output:\n{}", meanLossPerOutput);

    return meanLossPerOutput;
}

RowMatrix
CascadeCorrelationNetwork::predict(const RowMatrix& x) const {
    checkInputAssertions(x);

    auto outputActivationFunc = std::ref(*mOutputActivation);

    RowMatrix xWithHidden = calculateHiddenOutputs(addBias(x));

    RowMatrix activatedOutput = mOutputLayer.calculateOutput(xWithHidden)
            .unaryExpr(outputActivationFunc);

    return activatedOutput;
}

RowMatrix
CascadeCorrelationNetwork::calculateHiddenOutputs(const RowMatrix& x) const {
    RowMatrix xWithHidden(x.rows(), mInputDimension + mHiddenLayers.size());

    xWithHidden.leftCols(mInputDimension) = x;

    SizeType index = mInputDimension;
    for (auto&& hidden : mHiddenLayers) {
        xWithHidden.col(index) = hidden
                .calculateOutput(xWithHidden.leftCols(index))
                .unaryExpr(std::ref(*mHiddenActivation));

        ++index;
    }

    return xWithHidden;
}

Scalar
CascadeCorrelationNetwork::trainOutputLayer(const RowMatrix& x, const RowMatrix& y) {
    auto outputActivationFunc = std::cref(*mOutputActivation);
    auto outputDerivativeFunc = std::bind(&ContinuousFunction::derivative,
                                          outputActivationFunc,
                                          _1);

    auto lossFunc = std::cref(*mLossFunction);
    auto lossDerivativeFunc = std::bind(&ContinuousFunction::derivative, lossFunc, _1);

    RowMatrix xWithHidden = calculateHiddenOutputs(x);
    RowMatrix rawOutput = mOutputLayer.calculateOutput(xWithHidden);

    mLogger->debug("Input batch with hidden:\n{}", xWithHidden);

    RowMatrix activatedOutput = rawOutput
            .unaryExpr(outputActivationFunc);

    mLogger->debug("Network output:\n{}", activatedOutput);

    auto rawErrorValuesExpr = y - activatedOutput;

    RowMatrix meanLossValues = rawErrorValuesExpr
            .unaryExpr(lossFunc)
            .colwise().mean();
    RowMatrix lossDerivatives = rawErrorValuesExpr
            .unaryExpr(lossDerivativeFunc);

    mLogger->debug("Mean loss values:\n{}", meanLossValues);

    RowMatrix activationDerivatives = rawOutput.unaryExpr(outputDerivativeFunc);

    MatrixView optimizedWeights = mOutputLayer.weights();
    mOutputOptimizer->optimize(
            lossDerivatives.cwiseProduct(activationDerivatives),
            xWithHidden,
            optimizedWeights);

    mLogger->debug("Output weights after optimization:\n{}", optimizedWeights);

    return meanLossValues.mean();
}

SizeType
CascadeCorrelationNetwork::getNumHidden() const {
    return mHiddenLayers.size();
}

Scalar
CascadeCorrelationNetwork::trainNewHiddenLayer(const RowMatrix& x, const RowMatrix& y,
        layers::HiddenLayer& candidate) {
    auto hiddenActivationFunc = std::cref(*mHiddenActivation);
    auto outputActivationFunc = std::cref(*mOutputActivation);

    auto lossFunc = std::cref(*mLossFunction);

    auto hiddenDerivativeFunc = std::bind(&ContinuousFunction::derivative,
                                          hiddenActivationFunc,
                                          _1);

    auto signFunc = [](Scalar v) -> Scalar { return std::copysign(1., v); };

    auto absFunc = [](Scalar v) -> Scalar { return std::fabs(v); };

    // TODO clean up this mess
    mLogger->debug("Initial candidate weights:\n{}", candidate.weights());

    RowMatrix xWithHidden = calculateHiddenOutputs(x);
    RowMatrix rawOutput = mOutputLayer.calculateOutput(xWithHidden);

    auto activatedOutputExpr = rawOutput.unaryExpr(outputActivationFunc);
    auto rawErrorValuesExpr = y - activatedOutputExpr;

    RowMatrix lossValues = rawErrorValuesExpr.unaryExpr(lossFunc);

    RowMatrix lossValuesDeviation = lossValues.rowwise()
            - lossValues.colwise().mean();

    mLogger->debug("Loss value deviation:\n{}", lossValuesDeviation);

    ColVector rawCandidateOutput = candidate.calculateOutput(xWithHidden);

    RowMatrix candidateActivatedOutput = rawCandidateOutput
            .unaryExpr(hiddenActivationFunc);
    ColVector candidateOutputDerivatives = rawCandidateOutput
            .unaryExpr(hiddenDerivativeFunc);

    mLogger->debug("Candidate output:\n{}", candidateActivatedOutput);

    RowVector meanCandidateOutput = candidateActivatedOutput.colwise().mean();

    ColVector candidateOutputDeviation = candidateActivatedOutput.rowwise()
            - meanCandidateOutput;

    mLogger->debug("Mean candidate output:\n{}", meanCandidateOutput);

    mLogger->debug("Candidate output deviation:\n{}", candidateOutputDeviation);

    RowMatrix covariance = lossValuesDeviation.array().colwise()
            * candidateOutputDeviation.array();

    mLogger->debug("Covariance:\n{}", covariance);

    RowVector totalCovariance = covariance.colwise().sum()
            .unaryExpr(absFunc);

    RowMatrix covarianceSigns = covariance.unaryExpr(signFunc);

    auto signedLossDeviationExpr = lossValuesDeviation
            .cwiseProduct(covarianceSigns);

    RowMatrix deltas = (signedLossDeviationExpr.array().colwise()
            * candidateOutputDerivatives.array()).rowwise().sum();

    mLogger->debug("Covariance signs:\n{}", covarianceSigns);

    mLogger->debug("Total covariance (absolute):\n{}", totalCovariance);

    mLogger->debug("Deltas for optimization:\n{}", deltas);

    MatrixView optimizedWeights = candidate.weights();
    mHiddenOptimizer->optimize(deltas, xWithHidden, optimizedWeights);

    return totalCovariance.sum();
}

void
CascadeCorrelationNetwork::checkInputAssertions(const RowMatrix& x) const {
    // check for common errors
    if (x.cols() != mInputDimension - 1) {
        throw std::invalid_argument("Incorrect input data dimension");
    }
}

void
CascadeCorrelationNetwork::checkOutputAssertions(const RowMatrix& y) const {
    if (y.cols() != mOutputDimension) {
        throw std::invalid_argument("Incorrect label data dimension");
    }
}

void
CascadeCorrelationNetwork::checkSampleCounts(const RowMatrix& x, const RowMatrix& y) {
    if (x.rows() != y.rows()) {
        throw std::invalid_argument("Input and label dataset sizes do not match");
    }
}

RowMatrix
CascadeCorrelationNetwork::addBias(const RowMatrix& x) {
    const SizeType xRows = x.rows();
    const SizeType xCols = x.cols();

    RowMatrix xWithBias{xRows, xCols + 1};
    xWithBias.leftCols(xCols) = x;
    xWithBias.rightCols(1) = ColVector::Constant(xRows, BIAS_INPUT_VALUE);

    return xWithBias;
}

Scalar
CascadeCorrelationNetwork::trainOutputWithPatience(const RowMatrix& x, const RowMatrix& y,
        SizeType batchSize,
        History& history,
        unsigned int patience,
        Scalar tolerance,
        unsigned int safetyEpochLimit) {
    unsigned int patientEpochs = 0;
    Scalar prevLoss = 0.;

    for (unsigned int epoch = 1; patientEpochs < patience; ++epoch) {
        if (safetyEpochLimit && mTotalEpochs >= safetyEpochLimit) {
            return prevLoss;
        }

        mLogger->info("Output training epoch {}", epoch);

        const Scalar meanLoss = trainOutputOnBatches(x, y, batchSize);

        mLogger->info("Mean loss for epoch {}: {}", epoch, meanLoss);

        const Scalar lossDifference = std::fabs(meanLoss - prevLoss);
        prevLoss = meanLoss;

        history.addNewRecord(meanLoss);
        history.addHiddenCount(mHiddenLayers.size());

        mLogger->info("Absolute loss difference since last epoch: {}",
                      lossDifference);

        if (lossDifference < tolerance) {
            ++patientEpochs;
        } else {
            patientEpochs = 0;
        }

        ++mTotalEpochs;
    }

    return prevLoss;
}

void
CascadeCorrelationNetwork::trainNewHiddenWithPatience(const RowMatrix& x, const RowMatrix& y,
        SizeType batchSize,
        History& history,
        layers::HiddenLayer& candidate,
        unsigned int patience,
        Scalar tolerance,
        unsigned int safetyEpochLimit) {
    unsigned int patientEpochs = 0;
    Scalar prevCorrelation = 0.;

    HiddenHistory hiddenHistory;

    const size_t candidateNumber = mHiddenLayers.size() + 1;
    for (unsigned int epoch = 1; patientEpochs < patience; ++epoch) {
        if (safetyEpochLimit && mTotalEpochs >= safetyEpochLimit) {
            history.addHiddenHistory(std::move(hiddenHistory));
            return;
        }

        mLogger->info("Candidate number {}, training epoch: {}",
                      candidateNumber, epoch);

        const Scalar totalCorrelation = trainNewHiddenOnBatches(x, y, candidate, batchSize);

        mLogger->info("Total correlation for epoch {}: {}", epoch, totalCorrelation);

        const Scalar correlationDifference = std::fabs(totalCorrelation - prevCorrelation);
        prevCorrelation = totalCorrelation;

        mLogger->info("Absolute correlation difference since last epoch: {}",
                      correlationDifference);

        if (correlationDifference < tolerance) {
            ++patientEpochs;
        } else {
            patientEpochs = 0;
        }

        ++mTotalEpochs;
        hiddenHistory.addNewCovarianceRecord(totalCorrelation);
    }

    history.addHiddenHistory(std::move(hiddenHistory));
}

void
CascadeCorrelationNetwork::setLogLevel(LogLevel logLevel) {
    mLogger->set_level(static_cast<spdlog::level::level_enum>(logLevel));
}

Scalar CascadeCorrelationNetwork::trainOutputOnBatches(const RowMatrix& x, const RowMatrix& y,
        SizeType batchSize) {
    const utility::BatchRange xBatches{x, batchSize};
    const utility::BatchRange yBatches{y, batchSize};

    const SizeType nBatches = xBatches.getBatchNumber();
    Scalar accumulatedLoss = 0.;
    auto itX = xBatches.begin();
    auto itY = yBatches.begin();
    for (SizeType batchNumber = 1;
            itX != xBatches.end() && itY != yBatches.end();
            ++itX, ++itY, ++batchNumber) {
        mLogger->debug("Training output on batch {} out of {}", batchNumber, nBatches);
        mLogger->debug("Input batch:\n{}", *itX);
        mLogger->debug("Target batch:\n{}", *itY);
        accumulatedLoss += trainOutputLayer(*itX, *itY);
    }

    return accumulatedLoss / static_cast<Scalar>(nBatches);
}

Scalar CascadeCorrelationNetwork::trainNewHiddenOnBatches(const RowMatrix& x, const RowMatrix& y,
        layers::HiddenLayer& candidate,
        SizeType batchSize) {
    const utility::BatchRange xBatches{x, batchSize};
    const utility::BatchRange yBatches{y, batchSize};

    const SizeType nBatches = xBatches.getBatchNumber();
    Scalar accumulatedCovariance = 0.;
    auto itX = xBatches.begin();
    auto itY = yBatches.begin();
    for (SizeType batchNumber = 1;
            itX != xBatches.end() && itY != yBatches.end();
            ++itX, ++itY, ++batchNumber) {
        mLogger->debug("Training new hidden layer on batch {} out of {}", batchNumber, nBatches);
        mLogger->debug("Input batch:\n{}", *itX);
        mLogger->debug("Target batch:\n{}", *itY);
        accumulatedCovariance += trainNewHiddenLayer(*itX, *itY, candidate);
    }

    return accumulatedCovariance;
}
