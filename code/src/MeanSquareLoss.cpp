#include "loss/MeanSquareLoss.hpp"

using namespace cascrel;
using namespace cascrel::loss;

static const Scalar MEAN_SQUARE_CORRECTION = 0.5;

Scalar MeanSquareLoss::operator()(Scalar x) const {
    return MEAN_SQUARE_CORRECTION * x * x;
}

Scalar MeanSquareLoss::derivative(Scalar x) const {
    return (2 * MEAN_SQUARE_CORRECTION) * x;
}

LossFunction* MeanSquareLoss::clone() const {
    return new MeanSquareLoss(*this);
}
