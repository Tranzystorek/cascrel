#include "initializers/NormalInitializer.hpp"

#include <chrono>

using namespace cascrel;
using namespace cascrel::initializers;

NormalInitializer::NormalInitializer(Scalar mean, Scalar standardDeviation)
        : mRandomEngine(std::chrono::system_clock::now().time_since_epoch().count()),
          mNormalDistribution(mean, standardDeviation) {
}

NormalInitializer::NormalInitializer(unsigned seed, Scalar mean, Scalar standardDeviation)
        : mRandomEngine(seed),
          mNormalDistribution(mean, standardDeviation) {
}

Scalar NormalInitializer::operator()() {
    return mNormalDistribution(mRandomEngine);
}

Initializer* NormalInitializer::clone() const {
    return new NormalInitializer(*this);
}
