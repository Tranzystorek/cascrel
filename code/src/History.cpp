#include "History.hpp"

using namespace cascrel;

const std::vector<Scalar>& History::getLossRecords() const {
    return mLossRecords;
}

const std::vector<unsigned int>& History::getHiddenCounts() const {
    return mHiddenCounts;
}

const std::vector<HiddenHistory>& History::getHiddenHistories() const {
    return mHiddenHistories;
}

void History::addNewRecord(Scalar record) {
    mLossRecords.push_back(record);
}

void History::addHiddenCount(unsigned int count) {
    mHiddenCounts.push_back(count);
}

void History::addHiddenHistory(const HiddenHistory& hiddenHistory) {
    mHiddenHistories.push_back(hiddenHistory);
}

void History::addHiddenHistory(HiddenHistory&& hiddenHistory) {
    mHiddenHistories.push_back(hiddenHistory);
}
