#include "layers/HiddenLayer.hpp"

#include <functional>

using namespace cascrel;
using namespace cascrel::layers;

using cascrel::initializers::Initializer;

HiddenLayer::HiddenLayer(SizeType inDim, Initializer& initializer)
        : mWeights(ColVector::NullaryExpr(inDim,
                                          std::ref(initializer))) {
}

HiddenLayer::HiddenLayer(const ColVector& weights)
        : mWeights(weights) {
}

SizeType HiddenLayer::getInputDimension() const {
    return mWeights.rows();
}

SizeType HiddenLayer::getOutputDimension() const {
    return mWeights.ColsAtCompileTime;
}

RowMatrix HiddenLayer::calculateOutput(const RowMatrix& input) const {
    return input * mWeights;
}

MatrixView HiddenLayer::weights() {
    return MatrixView(mWeights.data(), mWeights.rows(),
            mWeights.ColsAtCompileTime);
}

void HiddenLayer::addRows(SizeType n) {
    // ignore parameter
    (void) n;
}
