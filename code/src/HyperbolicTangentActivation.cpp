#include "activation/HyperbolicTangentActivation.hpp"

#include <cmath>

using namespace cascrel;
using namespace cascrel::activation;

Scalar HyperbolicTangentActivation::operator()(Scalar x) const {
    return std::tanh(x);
}

Scalar HyperbolicTangentActivation::derivative(Scalar x) const {
    const Scalar hyperbolicTangent = std::tanh(x);

    return 1. - hyperbolicTangent * hyperbolicTangent;
}

ActivationFunction* HyperbolicTangentActivation::clone() const {
    return new HyperbolicTangentActivation(*this);
}
