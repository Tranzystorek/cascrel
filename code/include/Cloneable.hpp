#ifndef CASCREL_CLONEABLE_HPP
#define CASCREL_CLONEABLE_HPP

namespace cascrel {

class Cloneable {
public:
    virtual Cloneable* clone() const = 0;
};

} // namespace cascrel

#endif //CASCREL_CLONEABLE_HPP
