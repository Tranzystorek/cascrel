#ifndef CASCREL_BUILDER_HPP
#define CASCREL_BUILDER_HPP

#include <memory>
#include <utility>

#include "common.hpp"

#include "CascadeCorrelationNetwork.hpp"
#include "factory/Construct.hpp"
#include "activation/ActivationFunction.hpp"
#include "activation/LinearActivation.hpp"
#include "initializers/NormalInitializer.hpp"
#include "initializers/Initializer.hpp"
#include "loss/LossFunction.hpp"
#include "loss/MeanSquareLoss.hpp"
#include "optimizers/DeltaRuleOptimizer.hpp"
#include "optimizers/Optimizer.hpp"

static const cascrel::Scalar DEFAULT_HIDDEN_LEARNING_RATE = 0.001;

namespace cascrel {
namespace factory {

class Builder {
public:
    template <typename HiddenInitializer, typename... Args>
    Builder& withHiddenInitializer(Args&& ... args);

    template <typename OutputInitializer, typename... Args>
    Builder& withOutputInitializer(Args&& ... args);

    template <typename HiddenOptimizer, typename... Args>
    Builder& withHiddenOptimizer(Args&& ... args);

    template <typename OutputOptimizer, typename... Args>
    Builder& withOutputOptimizer(Args&& ... args);

    template <typename ActivationFunction, typename... Args>
    Builder& withHiddenActivation(Args&& ... args);

    template <typename ActivationFunction, typename... Args>
    Builder& withOutputActivation(Args&& ... args);

    template <typename LossFunction, typename... Args>
    Builder& withLossFunction(Args&& ... args);

    inline CascadeCorrelationNetwork
    buildWithDimensions(SizeType inputDimension, SizeType outputDimension);

private:
    std::unique_ptr<initializers::Initializer> hiddenInitializer;
    std::unique_ptr<initializers::Initializer> outputInitializer;
    std::unique_ptr<optimizers::Optimizer> hiddenOptimizer;
    std::unique_ptr<optimizers::Optimizer> outputOptimizer;
    std::unique_ptr<activation::ActivationFunction> hiddenActivation;
    std::unique_ptr<activation::ActivationFunction> outputActivation;
    std::unique_ptr<loss::LossFunction> lossFunction;
};

template <typename HiddenInitializer, typename... Args>
Builder& Builder::withHiddenInitializer(Args&& ... args) {
    hiddenInitializer.reset(
            Construct<initializers::Initializer>
            ::as<HiddenInitializer>(std::forward<Args>(args)...));

    return *this;
}

template <typename OutputInitializer, typename... Args>
Builder& Builder::withOutputInitializer(Args&& ... args) {
    outputInitializer.reset(
            Construct<initializers::Initializer>
            ::as<OutputInitializer>(std::forward<Args>(args)...));

    return *this;
}

template <typename HiddenOptimizer, typename... Args>
Builder& Builder::withHiddenOptimizer(Args&& ... args) {
    hiddenOptimizer.reset(
            Construct<optimizers::Optimizer>
            ::as<HiddenOptimizer>(std::forward<Args>(args)...));

    return *this;
}

template <typename OutputOptimizer, typename... Args>
Builder& Builder::withOutputOptimizer(Args&& ... args) {
    outputOptimizer.reset(
            Construct<optimizers::Optimizer>
            ::as<OutputOptimizer>(std::forward<Args>(args)...));

    return *this;
}

template <typename ActivationFunction, typename... Args>
Builder& Builder::withHiddenActivation(Args&& ... args) {
    hiddenActivation.reset(
            Construct<activation::ActivationFunction>
            ::as<ActivationFunction>(std::forward<Args>(args)...));

    return *this;
}

template <typename ActivationFunction, typename... Args>
Builder& Builder::withOutputActivation(Args&& ... args) {
    outputActivation.reset(
            Construct<activation::ActivationFunction>
            ::as<ActivationFunction>(std::forward<Args>(args)...));

    return *this;
}

template <typename LossFunction, typename... Args>
Builder& Builder::withLossFunction(Args&& ... args) {
    lossFunction.reset(
            Construct<loss::LossFunction>
            ::as<LossFunction>(std::forward<Args>(args)...));

    return *this;
}

CascadeCorrelationNetwork Builder::buildWithDimensions(
        SizeType inputDimension,
        SizeType outputDimension) {
    // set defaults where user did not explicitly select anything
    if (!hiddenInitializer) {
        hiddenInitializer.reset(
                Construct<initializers::Initializer>
                ::as<initializers::NormalInitializer>());
    }
    if (!outputInitializer) {
        outputInitializer.reset(
                Construct<initializers::Initializer>
                ::as<initializers::NormalInitializer>());
    }
    if (!hiddenOptimizer) {
        hiddenOptimizer.reset(
                Construct<optimizers::Optimizer>
                ::as<optimizers::DeltaRuleOptimizer>(
                        DEFAULT_HIDDEN_LEARNING_RATE));
    }
    if (!outputOptimizer) {
        outputOptimizer.reset(
                Construct<optimizers::Optimizer>
                ::as<optimizers::DeltaRuleOptimizer>());
    }
    if (!hiddenActivation) {
        hiddenActivation.reset(
                Construct<activation::ActivationFunction>
                ::as<activation::LinearActivation>());
    }
    if (!outputActivation) {
        outputActivation.reset(
                Construct<activation::ActivationFunction>
                ::as<activation::LinearActivation>());
    }
    if (!lossFunction) {
        lossFunction.reset(
                Construct<loss::LossFunction>
                ::as<loss::MeanSquareLoss>());
    }

    CascadeCorrelationNetwork buildResult(
            inputDimension, outputDimension,
            dynamic_cast<initializers::Initializer&>(*hiddenInitializer->clone()),
            dynamic_cast<initializers::Initializer&>(*outputInitializer->clone()),
            dynamic_cast<optimizers::Optimizer&>(*hiddenOptimizer->clone()),
            dynamic_cast<optimizers::Optimizer&>(*outputOptimizer->clone()),
            dynamic_cast<activation::ActivationFunction&>(*hiddenActivation->clone()),
            dynamic_cast<activation::ActivationFunction&>(*outputActivation->clone()),
            dynamic_cast<loss::LossFunction&>(*lossFunction->clone()));

    return buildResult;
}

} // namespace factory
} // namespace cascrel

#endif //CASCREL_BUILDER_HPP
