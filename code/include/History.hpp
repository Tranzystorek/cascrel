#ifndef CASCREL_HISTORY_HPP
#define CASCREL_HISTORY_HPP

#include <vector>

#include "common.hpp"
#include "HiddenHistory.hpp"

namespace cascrel {

class History {
public:
    const std::vector<Scalar>& getLossRecords() const;
    const std::vector<unsigned int>& getHiddenCounts() const;
    const std::vector<HiddenHistory>& getHiddenHistories() const;

private:
    void addNewRecord(Scalar record);
    void addHiddenCount(unsigned int count);
    void addHiddenHistory(const HiddenHistory& hiddenHistory);
    void addHiddenHistory(HiddenHistory&& hiddenHistory);

private:
    std::vector<Scalar> mLossRecords;
    std::vector<unsigned int> mHiddenCounts;
    std::vector<HiddenHistory> mHiddenHistories;

    friend class CascadeCorrelationNetwork;
};

} // namespace cascrel

#endif //CASCREL_HISTORY_HPP
