#ifndef CASCREL_HIDDENHISTORY_HPP
#define CASCREL_HIDDENHISTORY_HPP

#include <vector>

#include "common.hpp"

namespace cascrel {

class HiddenHistory {
public:
    const std::vector<Scalar>& getCovarianceRecords() const;

private:
    void addNewCovarianceRecord(Scalar record);

private:
    std::vector<Scalar> mCovarianceRecords;

    friend class CascadeCorrelationNetwork;
};

} // namespace cascrel

#endif //CASCREL_HIDDENHISTORY_HPP
