#ifndef CASCREL_DELTARULEOPTIMIZER_HPP
#define CASCREL_DELTARULEOPTIMIZER_HPP

#include "optimizers/Optimizer.hpp"

namespace cascrel {
namespace optimizers {

class DeltaRuleOptimizer : public Optimizer {
public:
    explicit DeltaRuleOptimizer(Scalar learningRate = 0.01);

    void optimize(const RowMatrix& derivatives, const RowMatrix& x,
            MatrixView& optimizedWeights) override;

    Optimizer* clone() const override;

private:
    Scalar mLearningRate;
};

} // namespace optimizers
} // namespace cascrel

#endif //CASCREL_DELTARULEOPTIMIZER_HPP
