#ifndef CASCREL_OPTIMIZER_HPP
#define CASCREL_OPTIMIZER_HPP

#include "common.hpp"

#include "Cloneable.hpp"

namespace cascrel {
namespace optimizers {

class Optimizer : public Cloneable {
public:
    virtual void optimize(const RowMatrix& derivatives, const RowMatrix& x,
            MatrixView& optimizedWeights) = 0;

    virtual ~Optimizer() = default;
};

} // namespace optimizers
} // namespace cascrel

#endif //CASCREL_OPTIMIZER_HPP
