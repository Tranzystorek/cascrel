#ifndef CASCREL_HIDDENLAYER_HPP
#define CASCREL_HIDDENLAYER_HPP

#include "common.hpp"

#include "initializers/Initializer.hpp"
#include "layers/Layer.hpp"

namespace cascrel {
namespace layers {

class HiddenLayer : public Layer {
public:
    HiddenLayer(SizeType inDim, initializers::Initializer& initializer);

    explicit HiddenLayer(const ColVector& weights);

    SizeType getInputDimension() const override;

    SizeType getOutputDimension() const override;

    RowMatrix calculateOutput(const RowMatrix& input) const override;

    MatrixView weights() override;

    /// No effect: in HiddenLayer weight matrix cannot be resized.
    /// \param n ignored
    void addRows(SizeType n) override;

private:
    ColVector mWeights;
};

} // namespace layers
} // namespace cascrel

#endif //CASCREL_HIDDENLAYER_HPP
