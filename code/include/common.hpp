#ifndef CASCREL_COMMON_HPP
#define CASCREL_COMMON_HPP

#include <Eigen/Core>
#include <spdlog/spdlog.h>

/// \file
/// Common aliases and constants

namespace cascrel {

enum LogLevel {
    DEBUG = spdlog::level::debug,
    INFO = spdlog::level::info,
    WARN = spdlog::level::warn,
    OFF = spdlog::level::off
};

using Scalar = double;
using SizeType = Eigen::Index;

using RowMatrix = Eigen::Matrix<
        Scalar,
        Eigen::Dynamic,
        Eigen::Dynamic,
        Eigen::RowMajor>;
using Matrix = Eigen::MatrixXd;

using MatrixView = Eigen::Map<Matrix>;

using RowVector = Eigen::RowVectorXd;
using ColVector = Eigen::VectorXd;

/// \var Scalar BIAS_INPUT_VALUE
/// \brief Bias value appended to every sample
const Scalar BIAS_INPUT_VALUE = 1.;

} // naemspace cascrel

#endif //CASCREL_COMMON_HPP
