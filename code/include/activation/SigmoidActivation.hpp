#ifndef CASCREL_SIGMOIDACTIVATION_HPP
#define CASCREL_SIGMOIDACTIVATION_HPP

#include "activation/ActivationFunction.hpp"

namespace cascrel {
namespace activation {

class SigmoidActivation : public ActivationFunction {
public:
    Scalar operator()(Scalar x) const override;

    Scalar derivative(Scalar x) const override;

    ActivationFunction* clone() const override;
};

} // namespace activation
} // namepsace cascrel

#endif //CASCREL_SIGMOIDACTIVATION_HPP
