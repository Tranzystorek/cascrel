#ifndef CASCREL_SOFTPLUSACTIVATION_HPP
#define CASCREL_SOFTPLUSACTIVATION_HPP

#include "activation/ActivationFunction.hpp"

namespace cascrel {
namespace activation {

class SoftplusActivation : public ActivationFunction {
public:
    Scalar operator()(Scalar x) const override;

    Scalar derivative(Scalar x) const override;

    ActivationFunction* clone() const override;
};

} // namespace activation
} // namespace cascrel

#endif //CASCREL_SOFTPLUSACTIVATION_HPP
