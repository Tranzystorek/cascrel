if(NOT CASCREL_USE_CONAN)
    find_package(Catch2 REQUIRED)
endif()

add_executable(
        cascrel_test
        EXCLUDE_FROM_ALL
        src/test-main.cpp
        src/OutputLayerTest.cpp
        src/HiddenLayerTest.cpp
        src/ConstructTest.cpp
        src/BatchRangeTest.cpp
)

target_include_directories(cascrel_test PRIVATE include)

if(CASCREL_USE_CONAN)
    target_link_libraries(cascrel_test PRIVATE cascrel CONAN_PKG::Catch2)
else()
    target_link_libraries(cascrel_test PRIVATE cascrel Catch2::Catch2)
endif()

add_test(NAME CascrelTest COMMAND cascrel_test -r compact)
add_custom_target(
        check
        COMMAND ${CMAKE_CTEST_COMMAND}
        DEPENDS cascrel_test)
