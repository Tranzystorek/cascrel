#include "layers/HiddenLayer.hpp"
#include "CatchTestConvenience.hpp"

#include <memory>

#include "factory/Construct.hpp"
#include "initializers/NormalInitializer.hpp"
#include "initializers/Initializer.hpp"

static const cascrel::SizeType INPUT_DIMENSION = 3;
static const cascrel::SizeType OUTPUT_DIMENSION = 1;

using cascrel::factory::Construct;
using cascrel::initializers::NormalInitializer;
using cascrel::initializers::Initializer;
using cascrel::layers::HiddenLayer;

class HiddenLayerTest {
public:
    HiddenLayerTest()
            : initializer(
            Construct<Initializer>::as<NormalInitializer>()),
              hiddenLayer(INPUT_DIMENSION, *initializer) {
        setLayerWeights();
    }

protected:
    std::unique_ptr<Initializer> initializer;
    HiddenLayer hiddenLayer;

private:
    void setLayerWeights() {
        cascrel::ColVector weights{INPUT_DIMENSION};
        weights <<
                10,
                11,
                12;

        hiddenLayer = HiddenLayer(weights);
    }
};

TC_METHOD_WITH_CLASS_NAME(HiddenLayerTest, ShouldReturnInputDimension) {
    REQUIRE_EQ(INPUT_DIMENSION, hiddenLayer.getInputDimension());
}

TC_METHOD_WITH_CLASS_NAME(HiddenLayerTest, ShouldReturnOutputDimension) {
    REQUIRE_EQ(OUTPUT_DIMENSION, hiddenLayer.getOutputDimension());
}

TC_METHOD_WITH_CLASS_NAME(HiddenLayerTest, ShouldCalculateInputCorrectly) {
    cascrel::RowMatrix v{1, INPUT_DIMENSION};
    v << 1, 2, 3;

    cascrel::RowMatrix expectedOutput{1, OUTPUT_DIMENSION};
    expectedOutput << 1 * 10 + 2 * 11 + 3 * 12;

    REQUIRE_EQ(expectedOutput, hiddenLayer.calculateOutput(v));
}

TC_METHOD_WITH_CLASS_NAME(HiddenLayerTest, ShouldReturnCorrectMatrixView) {
    cascrel::Matrix expectedWeights{INPUT_DIMENSION, OUTPUT_DIMENSION};
    expectedWeights <<
            10,
            11,
            12;

    REQUIRE_EQ(expectedWeights, hiddenLayer.weights());
}

TC_METHOD_WITH_CLASS_NAME(HiddenLayerTest,
                          ShouldReturnModifiedWeightsWhenModified) {
    cascrel::ColVector expectedWeights{INPUT_DIMENSION};
    expectedWeights <<
            10,
            -1,
            12;

    const cascrel::SizeType modifiedIndex = 1;
    const cascrel::Scalar newValue = -1;
    hiddenLayer.weights()(modifiedIndex) = newValue;

    REQUIRE_EQ(expectedWeights, hiddenLayer.weights());
}

TC_METHOD_WITH_CLASS_NAME(HiddenLayerTest, ShouldRetainSizeWhenResized) {
    const cascrel::SizeType rowSizeIncrease = 5;
    const cascrel::SizeType expectedRowSize = hiddenLayer.getInputDimension();

    hiddenLayer.addRows(rowSizeIncrease);

    REQUIRE_EQ(expectedRowSize, hiddenLayer.getInputDimension());
}
