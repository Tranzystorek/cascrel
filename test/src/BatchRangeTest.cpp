#include "utility/BatchRange.hpp"
#include "CatchTestConvenience.hpp"

#include "common.hpp"

using cascrel::utility::BatchRange;

class BatchRangeTest {};

TC_METHOD_WITH_CLASS_NAME(BatchRangeTest, ShouldIterateWhenEvenBatches) {
    cascrel::RowMatrix matrix{6, 2};
    matrix << 1, 1,
              2, 2,
              3, 3,
              4, 4,
              5, 5,
              6, 6;

    const BatchRange range{matrix, 2};
    auto it = range.begin();

    cascrel::RowMatrix expectedFirstBatch{2, 2};
    expectedFirstBatch << 1, 1,
                          2, 2;
    REQUIRE_EQ(expectedFirstBatch, *it++);

    cascrel::RowMatrix expectedSecondBatch{2, 2};
    expectedSecondBatch << 3, 3,
                           4, 4;
    REQUIRE_EQ(expectedSecondBatch, *it++);

    cascrel::RowMatrix expectedThirdBatch{2, 2};
    expectedThirdBatch << 5, 5,
                          6, 6;
    REQUIRE_EQ(expectedThirdBatch, *it++);

    REQUIRE_EQ(range.end(), it);
}

TC_METHOD_WITH_CLASS_NAME(BatchRangeTest, ShouldIterateWhenUnvenBatches) {
    cascrel::RowMatrix matrix{5, 2};
    matrix << 1, 1,
              2, 2,
              3, 3,
              4, 4,
              5, 5;

    const BatchRange range{matrix, 2};
    auto it = range.begin();

    cascrel::RowMatrix expectedFirstBatch{2, 2};
    expectedFirstBatch << 1, 1,
                          2, 2;
    REQUIRE_EQ(expectedFirstBatch, *it++);

    cascrel::RowMatrix expectedSecondBatch{2, 2};
    expectedSecondBatch << 3, 3,
                           4, 4;
    REQUIRE_EQ(expectedSecondBatch, *it++);

    cascrel::RowMatrix expectedThirdBatch{1, 2};
    expectedThirdBatch << 5, 5;
    REQUIRE_EQ(expectedThirdBatch, *it++);

    REQUIRE_EQ(range.end(), it);
}

TC_METHOD_WITH_CLASS_NAME(BatchRangeTest, ShouldCallUnderlyingMethodWhenArrowOperatorUsed) {
    cascrel::RowMatrix matrix{6, 2};
    matrix << 1, 1,
              2, 2,
              3, 3,
              4, 4,
              5, 5,
              6, 6;

    const BatchRange range{matrix, 3};
    for (auto it = range.begin(); it != range.end(); ++it) {
        REQUIRE_EQ(3, it->rows());
    }
}
