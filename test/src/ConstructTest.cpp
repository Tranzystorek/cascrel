#include "factory/Construct.hpp"
#include "CatchTestConvenience.hpp"

#include <memory>
#include <typeinfo>

#include "activation/ActivationFunction.hpp"
#include "activation/HyperbolicTangentActivation.hpp"
#include "activation/LinearActivation.hpp"
#include "activation/SigmoidActivation.hpp"
#include "initializers/NormalInitializer.hpp"
#include "initializers/Initializer.hpp"
#include "loss/LossFunction.hpp"
#include "loss/MeanSquareLoss.hpp"
#include "optimizers/DeltaRuleOptimizer.hpp"
#include "optimizers/Optimizer.hpp"

using cascrel::activation::ActivationFunction;
using cascrel::activation::HyperbolicTangentActivation;
using cascrel::activation::LinearActivation;
using cascrel::activation::SigmoidActivation;
using cascrel::factory::Construct;
using cascrel::initializers::NormalInitializer;
using cascrel::initializers::Initializer;
using cascrel::loss::LossFunction;
using cascrel::loss::MeanSquareLoss;
using cascrel::optimizers::DeltaRuleOptimizer;
using cascrel::optimizers::Optimizer;

class ConstructTest {
protected:
    std::unique_ptr<ActivationFunction> activationFunction;
    std::unique_ptr<LossFunction> lossFunction;
    std::unique_ptr<Optimizer> optimizer;
    std::unique_ptr<Initializer> initializer;
};

TC_METHOD_WITH_CLASS_NAME(ConstructTest, ShouldCreateLinearActivation) {
    activationFunction.reset(
            Construct<ActivationFunction>::as<LinearActivation>());

    const size_t expectedHashCode = typeid(LinearActivation).hash_code();

    REQUIRE_EQ(expectedHashCode, typeid(*activationFunction).hash_code());
}

TC_METHOD_WITH_CLASS_NAME(ConstructTest, ShouldCreateSigmoidActivation) {
    activationFunction.reset(
            Construct<ActivationFunction>::as<SigmoidActivation>());

    const size_t expectedHashCode = typeid(SigmoidActivation).hash_code();

    REQUIRE_EQ(expectedHashCode, typeid(*activationFunction).hash_code());
}

TC_METHOD_WITH_CLASS_NAME(ConstructTest,
                          ShouldCreateHyperbolicTangentActivation) {
    activationFunction.reset(
            Construct<ActivationFunction>::as<HyperbolicTangentActivation>());

    const size_t expectedHashCode = typeid(HyperbolicTangentActivation)
            .hash_code();

    REQUIRE_EQ(expectedHashCode, typeid(*activationFunction).hash_code());
}

TC_METHOD_WITH_CLASS_NAME(ConstructTest, ShouldCreateMeanSquareLoss) {
    lossFunction.reset(
            Construct<LossFunction>::as<MeanSquareLoss>());

    const size_t expectedHashCode = typeid(MeanSquareLoss).hash_code();

    REQUIRE_EQ(expectedHashCode, typeid(*lossFunction).hash_code());
}

TC_METHOD_WITH_CLASS_NAME(ConstructTest, ShouldCreateNormalInitializer) {
    initializer.reset(
            Construct<Initializer>::as<NormalInitializer>());

    const size_t expectedHashCode = typeid(NormalInitializer).hash_code();

    REQUIRE_EQ(expectedHashCode, typeid(*initializer).hash_code());
}

TC_METHOD_WITH_CLASS_NAME(ConstructTest, ShouldCreateDeltaRuleOptimizer) {
    optimizer.reset(Construct<Optimizer>::as<DeltaRuleOptimizer>());

    const size_t expectedHashCode = typeid(DeltaRuleOptimizer).hash_code();

    REQUIRE_EQ(expectedHashCode, typeid(*optimizer).hash_code());
}
